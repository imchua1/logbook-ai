| Wiskunde                                    | Kennis  | Toepassen | Analyse | Creatie | Link naar bewijs |
|---------------------------------------------|---------|-----------|---------|---------|------------------|
| Matrixvermenigvuldigen                      |   Yep   |  no
| Determinant                                 |
| Partieel differentiëren                     |
| Actievatiefuncties                          |
| Inverse matrix                              |
| Transpose Matrix                            |
| Inproduct                                   |
| Euclidische afstand                         |


| Python                                      | Kennis  | Toepassen | Analyse | Creatie | Link naar bewijs |
|---------------------------------------------|---------|-----------|---------|---------|------------------|
| Comprehensions                              |
| Dictionaries                                |
| Slicing                                     |
| Pandas                                      |
| Numpy                                       |
| Keras                                       |
| Mathplotlib                                 |
| Scikit\-learn                               |


| Statistics                                  | Kennis  | Toepassen | Analyse | Creatie | Link naar bewijs |
|---------------------------------------------|---------|-----------|---------|---------|------------------|
| Mediaan                                     |
| normale verdeling                           |
| Z scores                                    |
| standaardafwijking                          |
| parametrische en non parametrische toetsen  |
| ANOVA \(Kmeans\)                            |
| t toets                                     |
| chi square \(Tree\)                         |
| regressie analyse                           |
| factor analyse                              |
| Kansrekenen                                 |


| Datascience                                 | Kennis  | Toepassen | Analyse | Creatie | Link naar bewijs |
|---------------------------------------------|---------|-----------|---------|---------|------------------|
| Scatter Matrix                              |
| Data\-aquisitie                             |
| Dataselectie, opschoning                    |
| Visualisatie, interpretatie data            |
| Basics, Datastructuren                      |
| Correlatiematrix                            |


| Machine learning                            | Kennis  | Toepassen | Analyse | Creatie | Link naar bewijs |
|---------------------------------------------|---------|-----------|---------|---------|------------------|
| Supervised learning                         |
| Unsupervised learning                       |
| Feature selection                           |
| Classifiers                                 |
| Regressie                                   |
| Clustering                                  |
| Accuracy, precision, recall                 |
| ROC curve                                   |
| KNN                                         |
| SVM                                         |
| Naive Bayes                                 |
| Logistic Regression                         |
| Random Forest                               |
| Decision Tree\(s\)                          |
| Ensemble methods                            |
| Cross validation                            |
| Overfitting, underfitting                   |
| Kiezen van de juiste methode                |


| Deep learning                               | Kennis  | Toepassen | Analyse | Creatie | Link naar bewijs |
|---------------------------------------------|---------|-----------|---------|---------|------------------|
| Dense networks                              |
| Evaluatiefuncties                           |
| Stochastic gradient descent                 |
| Backpropagation                             |
| Learning curve                              |
| Autencoders                                 |
| Convolutional                               |
| Recurrent nets                              |
| Residual nets                               |
| Reinforcement learning                      |


| Toepassingen                                | Kennis  | Toepassen | Analyse | Creatie | Link naar bewijs |
|---------------------------------------------|---------|-----------|---------|---------|------------------|
| Deepspeech                                  |
| tekstmining                                 |
| language modelling                          |
| intent parsing                              |
| Dialogue                                    |
| Real\-time AI                               |
| Social robotics                             |
| Image recognition                           |
| Robotics/Control                            |
| InceptionV3                                 |


| Ethiek                                      | Kennis  | Mening    | Analyse | Creatie | Link naar bewijs |
|---------------------------------------------|---------|-----------|---------|---------|------------------|
| …                                           |
| …                                           |


| Personal                                    | Kennis  | Toepassen | Analyse | Creatie | Link naar bewijs |
|---------------------------------------------|---------|-----------|---------|---------|------------------|
| …                                           |
| …                                           |
